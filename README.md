## rabbitmq
### 消息中间件 -消息队列
- 异步
### 开发语言erlang 爱立信公司

#### 1、安装erlang：由于rabbitMq需要erlang语言的支持，在安装rabbitMq之前需要安装erlang
>
```python    
sudo apt-get install erlang-nox
```  
#### 2、安装、启动、停止、状态Rabbitmq
>
```python
    sudo apt-get update                     
    sudo apt-get install rabbitmq-server  安装
    
    /etc/init.d/rabbitmq-server restart   启动
    sudo rabbitmq-server start          
    sudo rabbitmq-server stop
    sudo rabbitmq-server restart
    sudo rabbitmqctl status
```
#### 3、添加用户，并赋予administrator权限
>
```python   
 添加admin用户，密码设置为admin。
        sudo rabbitmqctl add_user  admin  admin  
    赋予权限
        sudo rabbitmqctl set_user_tags admin administrator 
    赋予virtual host中所有资源的配置、写、读权限以便管理其中的资源
        sudo rabbitmqctl  set_permissions -p / admin '.*' '.*' '.*'
```    
#### 4、Web管理器连接
>
```python
    rabbitmqctl start_app
    rabbitmq-plugins enable rabbitmq_management
    rabbitmqctl stop
    浏览器访问http://192.168.137.3:15672（我的ubuntu地址是192.168.137.3）

    rabbitmqctl list_queues 查看队列
```

#### 5、消息的安全接收
>
    如何安全的吃包子

    消费者端吃完包子返回包子标识符
    ch.basic_ack(delivery_tag=method.delivery_tag)
    # no_ack=True


#### 6、持久化
>
    队列持久化
    channel.queue_declare(queue='task_queue',durable=True)
    消息持久化  前提是队列持久化
    channel.basic_publish(exchange='',
                          routing_key='task_queue',
                          body=message,
                          properties=pika.BasicProperties(
                              delivery_mode=2,  # make message persistent
                       
 )
 - RPC
 ![RPC](images/RPC.png)
- 启动web管理员界面
 ![启动web管理员界面](images/启动web管理页面.png)
- 过滤消息
 ![过滤消息](images/过滤消息.png)
- 更细致的过滤消息
 ![更细致的过滤消息](images/更细致的过滤消息.png)
 


