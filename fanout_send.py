# -*- coding: utf-8 -*-

"""
@Datetime: 2018/11/6
@Author: Zhang Yafei
"""
import pika
import sys

credentials = pika.PlainCredentials('admin','admin')
connection = pika.BlockingConnection(pika.ConnectionParameters(
    '192.168.137.3',5672,'/',credentials))
channel = connection.channel()

channel.exchange_declare(exchange='logs',exchange_type='fanout')   #声明交换机为logs,类型为广播，所有绑定到交换机上的队列都能接收到消息

message = ' '.join(sys.argv[1:]) or "info: Hello World!"
channel.basic_publish(exchange='logs',
                      routing_key='',
                      body=message)
print(" [x] Sent %r" % message)
connection.close()